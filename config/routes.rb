Rails.application.routes.draw do
  devise_for :models

  devise_for :users
  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
    get '/users/sign_in' => 'devise/sessions#new'
  end


  resources :comment

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :posts do
  # get '/posts', to: 'posts#index', as: :posts
  # get '/posts/new', to: 'posts#new', as: :new_post
  # get '/posts/:id', to: 'posts#show', as: :post
  # post '/posts', to: 'posts#create'
  # get '/posts/:id/edit', to: 'posts#edit', as: :edit_post
  # patch '/posts/:id', to: 'posts#update'
  # delete '/posts/:id', to: 'posts#delete'
    post 'comments', to: 'comment#create'
  end

  get '/categories/:category_handle/posts', to: 'posts#index', as: :category_posts

  root to: 'posts#index'
end
